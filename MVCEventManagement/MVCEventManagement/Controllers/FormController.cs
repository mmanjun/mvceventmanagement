﻿using MVCEventManagement.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVCEventManagement.Controllers
{
    public class FormController : Controller
    {
        // GET: Form
        public ActionResult Index()
        {
                 FormModel _model = new FormModel {
                _CreateEvent= new CreateEventModel { EventDate=DateTime.Now, EventID="Test", EventName="MyFirstEvent" },
                _CreatApplication= new CreateApplicationModel { ApplicationName="", EventID="", ApprovalNeeded=true, ExpirationDate=DateTime.Now.AddDays(90) },
                _MyEvents = new MyEventsModels { }
            };
            return View(_model);
        }
        
              
        public ActionResult MyEvents()
        {
            MyEventsModels model = new MyEventsModels();
            return PartialView("_MyEvents", model);
        }
       
        public ActionResult CreateNewEvent()
        {
            CreateEventModel model = new CreateEventModel();
            return PartialView("_CreateNewEvent", model);
        }
        public ActionResult EventDetails()
        {
            EventDetailsModel model = new EventDetailsModel();
            return PartialView("EventDetails", model);
        }
        [HttpGet]
        public ActionResult CreateApplication(string eventId)
        {
            CreateApplicationModel model = new CreateApplicationModel();
            model.inputs.Add("Yes");
            model.inputs.Add("No");
            model.EventID = eventId = "1";
            return PartialView("_CreateApplication", model);
        }

        [HttpPost]
        public ActionResult CreateApplication(CreateEventModel data)
        {
            CreateApplicationModel model = new CreateApplicationModel();
            model.EventID = data.EventID;
            List<string> items = new List<string> { "Yes", "No" };
            model.inputs = items;
            return PartialView("_CreateApplication",model);
        }       
        [HttpPost]
        public ActionResult ApplicationBuilder(CreateApplicationModel applicationdata)
        {
            return PartialView("ApplicationBuilder");
        }
        [HttpPost]
        public ActionResult Save(string inputs)
        {
            //check for null
            return View("Index");
        }

        public ActionResult Notification()
        {
            return PartialView("Notifications");
        }
        //public ActionResult MyEvents()
        //{

        //    List<MyEventsModels> DataList = new List<MyEventsModels>();
        //    DataList.Add(new MyEventsModels
        //    {
        //        EventName = "PDC_Application",
        //        IsActive = true,
        //        Submissions = 6,
        //        EventDetails = "Edit Details",
        //        CustomEmails = "Edit Emails",
        //        ApprovalsNeeded = true,
        //        EventDate = DateTime.Now               
        //    });
        //    DataList.Add(new MyEventsModels
        //    {
        //        EventName = "Webinar",
        //        IsActive = true,
        //        Submissions = 34,
        //        EventDetails = "Edit Details",
        //        CustomEmails = "Edit Emails",
        //        ApprovalsNeeded = true,
        //        EventDate = DateTime.Now
        //    });
        //    DataList.Add(new MyEventsModels
        //    {
        //        EventName = "RROC Application",
        //        IsActive = false,
        //        Submissions = 11,
        //        EventDetails = "Edit Details",
        //        CustomEmails = "Edit Emails",
        //        ApprovalsNeeded = false,
        //        EventDate = DateTime.Now
        //    });
        //    DataList.Add(new MyEventsModels
        //    {
        //        EventName = "MTC Pre-Survey",
        //        IsActive = false,
        //        Submissions = 21,
        //        EventDetails = "Edit Details",
        //        CustomEmails = "Edit Emails",
        //        ApprovalsNeeded = false,
        //        EventDate = DateTime.Now
        //    });

        //    DataList.Add(new MyEventsModels
        //    {
        //        EventName = "MTC Mentor Training",
        //        IsActive = true,
        //        Submissions = 6,
        //        EventDetails = "Edit Details",
        //        CustomEmails = "Edit Emails",
        //        ApprovalsNeeded = true,
        //        EventDate = DateTime.Now
        //    });
        //    return View(DataList);
        //}

        // GET: Form/Details/5
        //public ActionResult Details(int id)
        //{
        //    return View();
        //}

        //// GET: Form/Create
        //public ActionResult Create()
        //{
        //    return View();
        //}

        //// POST: Form/Create
        //[HttpPost]
        //public ActionResult Create(FormCollection collection)
        //{
        //    try
        //    {
        //        // TODO: Add insert logic here

        //        return RedirectToAction("Index");
        //    }
        //    catch
        //    {
        //        return View();
        //    }
        //}

        //// GET: Form/Edit/5
        //public ActionResult Edit(int id)
        //{
        //    return View();
        //}

        //// POST: Form/Edit/5
        //[HttpPost]
        //public ActionResult Edit(int id, FormCollection collection)
        //{
        //    try
        //    {
        //        // TODO: Add update logic here

        //        return RedirectToAction("Index");
        //    }
        //    catch
        //    {
        //        return View();
        //    }
        //}

        //// GET: Form/Delete/5
        //public ActionResult Delete(int id)
        //{
        //    return View();
        //}

        //// POST: Form/Delete/5
        //[HttpPost]
        //public ActionResult Delete(int id, FormCollection collection)
        //{
        //    try
        //    {
        //        // TODO: Add delete logic here

        //        return RedirectToAction("Index");
        //    }
        //    catch
        //    {
        //        return View();
        //    }
        //}
    }
}
