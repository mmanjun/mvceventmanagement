﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVCEventManagement.Models
{ 
    public class MyEventsModels
    {
        public string EventName { get; set; }
        public bool IsActive { get; set; }
        public int Submissions { get; set; }
        public string EventDetails { get; set; }
        public string CustomEmails { get; set; }
        public bool ApprovalsNeeded { get; set; }
        public DateTime EventDate { get; set; }
    }
}