﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace MVCEventManagement.Models
{
    public class CreateEventModel
    {
        public string EventName { get; set; }
        
        public string EventID { get; set; }

        public DateTime EventDate { get; set; }

    }
}