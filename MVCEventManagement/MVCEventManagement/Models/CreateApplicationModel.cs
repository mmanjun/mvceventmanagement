﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace MVCEventManagement.Models
{
    public class CreateApplicationModel
    {
        public string ApplicationName { get; set; }

        public string EventID { get; set; }

        public DateTime ExpirationDate { get; set; }

        public bool ApprovalNeeded { get; set; }

        public List<string> inputs { get; set; }
    }
}