﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVCEventManagement.Models
{
    public class NotificationsModel
    {
        public string acceptanceEmail { get; set; }
        public string  rejectionEmail { get; set; }
    }
}