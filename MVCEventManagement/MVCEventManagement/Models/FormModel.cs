﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;

namespace MVCEventManagement.Models
{
    public class FormModel
    {
        public MyEventsModels _MyEvents { get; set; }
        public CreateEventModel _CreateEvent { get; set; }
        public CreateApplicationModel _CreatApplication { get; set; }
        public NotificationsModel _Notifications { get; set; }

        public EventDetailsModel _EventDetails { get; set; }
    }
}