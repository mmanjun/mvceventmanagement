﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVCEventManagement.Models
{
    public class EventDetailsModel
    {
        public string  EventInformation { get; set; }
        public string  EventVenue { get; set; }
        public string  EventContactInfo { get; set; }
    }
}